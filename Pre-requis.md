# Pré-requis

## Logistique

Voici les éléments logistiques nécessaires pour le déroulement de la journée.

1. Une salle avec video-projecteur + prises electriques (1/personne minimum)
2. Chaque participant amène son poste de travail (ceci permet de travailler "pour de vrai", et identifier en direct d'éventuels problèmes invisibles autrement)
3. Chaque participant doit avoir un accès à internet. À destination des services informatiques, les flux nécessaires sont :
    * Port 22 vers `gitlab.adullact.net` pour faire du `git+ssh`
    * Ports 80 et 443 vers l'internet
4. Un accès internet pour l'intervenant Adullact, avec les caractéristiques ci-dessus.

## Questions

* Décrivez brievement votre poste
* (à destination des informaticiens) Quel éditeur de texte ou environnement de développement vous utilisez.
* (à destination des informaticiens) Quel système d'exploitation vous utilisez (et sa version).
* (à destination des informaticiens) Comment vous servez-vous de git : en ligne de commande ? avec un outil tiers ? (si oui lequel ?)

## Préparatifs techniques (à destination des informaticiens)

* se [créer un compte sur Adullact.net](https://adullact.net/account/register.php)
* se connecter sur Adullact.net (note: la confirmation du compte n'est pas une connexion, il est important de bien se reconnecter après avoir confirmé son compte).

Sur son poste de travail Windows :

* [Télécharger Git](https://git-scm.com/download/win) et l'installer
* Créer une clée SSH avec Git Bash : `ssh-keygen -t rsa -b 4096` (conserver les chemins proposés par défaut)
* Se connecter sur [gitlab.adullact.net](http://gitlab.adullact.net/)
* Suivre les instruction pour ajouter sa clé **publique** SSH (la clée étant créé, il n'y a plus qu'à copier la partie publique à l'emplacement dédié dans le Gitlab)
* Définir son identité : `git config --global user.name "Prénom NOM"`
* Définir son courriel : `git config --global user.email "moi@moncourriel.fr"`

Pour vérifier que tout est correct, merci de lancer depuis le *Git Bash* la commande `git clone git@gitlab.adullact.net:adullact/Accompagnement-git-gitlab.git`, et d'envoyer le résultat par courriel à Matthieu FAURE

## Proxy

Pour configurer un proxy pour git il convient de taper :

```
git config --global http.proxy http://proxyuser:proxypwd@proxy.server.com:proxyport
```

Exemple : `git config --global http.proxy http://mfaure:motdepasse@proxy.adullact.org:8080`